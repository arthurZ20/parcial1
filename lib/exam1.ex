defmodule Exam1 do
  # ============================
  # PARTE 1
  # ============================
  def funny_sort(_str) do
  end

  # ============================
  # PARTE 2
  # ============================
  def count([], _char) do
    0
  end

  # def count(seq, _char) do
  #   # IO.inspect(seq)
  #   Enum.to_list(seq)
  #   |> IO.inspect(seq)
  #   |> String.codepoints(seq)

  #   # if String.contains?(seq, char) && char == "C" do
  #   #   1 + count(seq, char)
  #   # end
  # end

  # TESTS DEL COUNT
  def count(seq, char) do
    # Cuenta cuantas veces aparacere en la sequencia
    # Con condicional que el caracter a buscar se el mismo que el char(aminoacido)
    Enum.count(seq, fn caracter -> caracter == char end)
  end

  # Para el caso base cuando no hay nucleotidos
  def histogram([]) do
    %{?A => 0, ?T => 0, ?C => 0, ?G => 0}
  end

  def histogram(seq) do
    seq
    |> Enum.reduce(%{?A => 0, ?T => 0, ?C => 0, ?G => 0}, fn char, acc ->
      # Si el char esta en la seq dada por el redude, no hay actualizacion del acc, pero si no esta el acc actualiza y da como valor predeterminado, y pasa al siguiente
      Map.update!(acc, char, &(&1 + 1))
    end)
  end

end
